<?php

// Load include files
module_load_include('nonces.inc', 'ctc');
module_load_include('widgets.inc', 'ctc');
module_load_include('api.inc', 'ctc');


/**
 * Implement hook_init
 */
function ctc_init() {
    if(!empty($_GET['user_identifier']) && !empty($_GET['user_nonce'])) {
        setcookie('USER_IDENTIFIER', $_GET['user_identifier'], time() + 60 * 60, '/');
        setcookie('USER_NONCE', $_GET['user_nonce'], time() + 60 * 60, '/');
        drupal_goto(current_path());
    }
}


/**
 * Implement hook_menu
 *
 * @return array
 */
function ctc_menu() {
    $menu = array();

    // Define system settings page
    $menu['admin/config/system/ctc'] = array(
        'title' => 'Campaigning Tools Connector',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('ctc_settings_form'),
        'access arguments' => array('administer ctc'),
    );

    $menu['logout'] = array (
        'title' => 'logout',
        'page callback'  => 'ctc_user_logout',
        'access arguments' => array('access content')
    );

    return $menu;
}

function ctc_user_logout() {

    $ctcCookies = array(
        'USER_IDENTIFIER',
        'USER_NONCE',
    );

    foreach ($ctcCookies as $ctcCookie) {
        if(isset($_COOKIE[$ctcCookie])) {
            unset($_COOKIE[$ctcCookie]);
            setcookie($ctcCookie, '', time() - 3600); // empty value and old timestamp
        }
    }

    drupal_goto('<front>');
}


/**
 * Implement hook_permission
 *
 * @return array
 */
function ctc_persmission() {
    return array(
        'administer ctc' => array(
            'title' => t('Administer Campaigning Tools Connector'),
            'description' => t('Manage IDs and more')
        )
    );
}


/**
 * Setup CTC System settings form
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function ctc_settings_form($form, &$form_state) {
    $form = system_settings_form($form);

    $form['ctc_settings_host'] = array(
        '#title' => t('Tools Host'),
        '#type' => 'textfield',
        '#default_value' => variable_get('ctc_settings_host')
    );
    $form['ctc_settings_cid'] = array(
        '#title' => t('Customer Identifier'),
        '#type' => 'textfield',
        '#default_value' => variable_get('ctc_settings_cid')
    );
    $form['ctc_settings_cs'] = array(
        '#title' => t('Customer Secret'),
        '#type' => 'textfield',
        '#default_value' => variable_get('ctc_settings_cs')
    );

    $form['ctc_settings_noncecount'] = array(
        '#title' => t('Number of nonces to load'),
        '#type' => 'textfield',
        '#default_value' => variable_get('ctc_settings_noncecount', 100)
    );

    $form['ctc_settings_development_mode'] = array(
        '#title' => t('Development mode'),
        '#type' => 'checkbox',
        '#default_value' => variable_get('ctc_settings_development_mode', false),
        '#description' => t('No caches will be used')
    );

    $form['ctc_settings_facebook'] = array(
        '#title' => t('Facebook'),
        '#type' => 'fieldset',

        'ctc_settings_facebook_app_id' => array(
            '#title' => t('Facebook app id'),
            '#type' => 'textfield',
            '#default_value' => variable_get('ctc_settings_facebook_app_id', '')
        )
    );

    $form['ctc_tableform'] = array(
        '#type' => 'tableform',
        '#header' => array(
            t('Module'),
            t('Identifier'),
        ),
        '#attributes' => array('style' => 'width: 55%;'),
        '#options' => array(
            'ctc_settings_storywall_module_identifier' => array(
                t('Storywall Module'),
                'ctc_settings_storywall_module_identifier' => array(
                    '#type' => 'textfield',
                    '#default_value' => variable_get('ctc_settings_storywall_module_identifier')
                )
            ),
            'ctc_settings_event_module_identifier' => array(
                t('Event Module'),
                'ctc_settings_event_module_identifier' => array(
                    '#type' => 'textfield',
                    '#default_value' => variable_get('ctc_settings_event_module_identifier')
                )
            ),
            'ctc_settings_commitment_module_identifier' => array(
                t('Commitment Module'),
                'ctc_settings_commitment_module_identifier' => array(
                    '#type' => 'textfield',
                    '#default_value' => variable_get('ctc_settings_commitment_module_identifier')
                )
            ),
            'ctc_settings_voting_module_identifier' => array(
                t('Voting Module'),
                'ctc_settings_voting_module_identifier' => array(
                    '#type' => 'textfield',
                    '#default_value' => variable_get('ctc_settings_voting_module_identifier')
                )
            ),
            'ctc_settings_buzzers_module_identifier' => array(
                t('Buzzres Module'),
                'ctc_settings_buzzers_module_identifier' => array(
                    '#type' => 'textfield',
                    '#default_value' => variable_get('ctc_settings_buzzers_module_identifier')
                )
            ),
        )
    );

    return $form;
}

/**
 * Return tools settings
 *
 * @param string $setting
 * @return array|string
 */
function ctc_get_settings($setting = '') {

    $settings = array(
        'HOST' => variable_get('ctc_settings_host'),
        'CID' => variable_get('ctc_settings_cid'),
        'SECRET' => variable_get('ctc_settings_cs')
    );

    if(!empty($setting)) {
        return $settings[$setting];
    }

    return $settings;
}

/**
 * Return tools module identifier
 *
 * @param $module string|bool to get a specific identifier
 *
 * @return array|string
 */
function ctc_get_module_identifier($module = false) {
    $modules = array(
        'STORYWALL_MODULE_IDENTIFIER' => variable_get('ctc_settings_storywall_module_identifier'),
        'EVENT_MODULE_IDENTIFIER' => variable_get('ctc_settings_event_module_identifier'),
        'COMMITMENT_MODULE_IDENTIFIER' => variable_get('ctc_settings_commitment_module_identifier'),
        'VOTING_MODULE_IDENTIFIER' => variable_get('ctc_settings_voting_module_identifier'),
        'BUZZERS_MODULE_IDENTIFIER' => variable_get('ctc_settings_buzzers_module_identifier'),
        'FACEBOOK_APP_ID' => variable_get('ctc_settings_facebook_app_id')
    );

    if($module) {
        return $modules[$module];
    }

    foreach($modules as $m => $value) {
        if(empty($value)) {
            unset($modules[$m]);
        }
    }

    return $modules;
}

/**
 * Implement hook_cron
 */
function ctc_cron() {
    // delete old nonces
    _ctc_clean_customer_nonces();

    // load new nonces
    _ctc_get_customer_nonces_from_api();
}
