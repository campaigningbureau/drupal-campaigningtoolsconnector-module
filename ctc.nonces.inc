<?php

/**
 * Return a valid nonce
 *
 * @return string
 */
function ctc_get_customer_nonce() {
    return _ctc_get_customer_nonce_from_db();
}

/**
 * Load a valid nonce from DB
 *
 * @param int $depth
 * @return string
 */
function _ctc_get_customer_nonce_from_db($depth = 0) {
    $nonceQuery = db_select('ctc_nonces', 'nonces')
        ->fields('nonces', array('id', 'customer_nonce'))
        ->condition('nonces.expiration_time', date('Y-m-d H:i:s'), '>')
        ->range(0,1)
        ->execute();
    $nonce = $nonceQuery->fetchAssoc();


    if(!empty($nonce)) {
        db_delete('ctc_nonces')->condition('id', $nonce['id'])->execute();
        return $nonce['customer_nonce'];
    }

    _ctc_get_customer_nonces_from_api();
    return (!$depth ? _ctc_get_customer_nonce_from_db($depth + 1) : false);
}

/**
 * Delete all expired nonces from DB
 *
 * @return DatabaseStatementInterface
 */
function _ctc_clean_customer_nonces() {
    return db_delete('ctc_nonces')->condition('expiration_time', date('Y-m-d H:i:s'), '<')->execute();
}


/**
 * Load new nonces from API
 *
 * @return bool|DatabaseStatementInterface|int|null
 */
function _ctc_get_customer_nonces_from_api() {
    $settings = ctc_get_settings();

    $host = $settings['HOST'];
    $cid = $settings['CID'];
    $secret = $settings['SECRET'];

    $count = variable_get('ctc_settings_noncecount', 100);

    if(empty($host) || empty($cid) || empty($secret)) {
        return false;
    }

    $url = $host . '/' . $cid . '/customer_nonce?count=' . $count . '&customer_secret=' . $secret;

    $response = drupal_http_request($url);

    if($response->code != '200') {
        watchdog('CTC', 'API returned code @code with data @data', array('@code' => $response->code, '@data' => isset($response->data) ? $response->data : ''), WATCHDOG_ERROR, $url);
    } else {
        $nonces = json_decode($response->data)->nonces;

        if(!empty($nonces)) {
            $query = db_insert('ctc_nonces')->fields(array('customer_nonce', 'expiration_time'));

            foreach($nonces as &$nonce) {
                $query->values((array)$nonce);
            }

            return $query->execute();
        }
    }

    return false;
}
