jQuery(function() {

    jQuery('[data-ctc-widget]').each(function(index, element) {
        var $element = jQuery(element),
            module = $element.data('ctc-widget');

        angular.bootstrap(element, [module]);
    })

});
