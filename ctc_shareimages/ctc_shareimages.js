/**
 * Shareimages Angular module
 *
 * Load images from API and handle user interaction
 */
;(function() {
    "use strict";

    if(!window.angular) return;


    var ctcShareImages = angular.module('ctc.shareimages', []);


    // Create the controller
    ctcShareImages.controller('shareimages', ["$scope", function($scope) {

        $scope.shareImages = [];
        $scope.activeShareImageKey = 0;

        $scope.showFacebookShare = false;
        $scope.facebookShareBody = '';

        /**
         * returns the active share image
         * @returns {*}
         */
        $scope.activeShareImage = function() {
            return $scope.shareImages[$scope.activeShareImageKey];
        };

        /**
         * increments the active key and checks if it´s not higher than available images
         */
        $scope.nextShareImage = function() {
            $scope.activeShareImageKey++;

            if($scope.activeShareImageKey >= $scope.shareImages.length) {
                $scope.activeShareImageKey = 0;
            }
        };

        /**
         * decrements the active key and checks if it´s not lower than null
         */
        $scope.prevShareImage = function() {
            $scope.activeShareImageKey--;

            if($scope.activeShareImageKey < 0) {
                $scope.activeShareImageKey = $scope.shareImages.length - 1;
            }
        };

        /**
         * Open The Twitter share window
         */
        $scope.sendTwitterShare = function() {

            var shareURL = $CB_TOOLS.HOST + '/' + $CB_TOOLS.CUSTOMER_IDENTIFIER + '/share_images/' + $scope.activeShareImage().id + '/vcard';
                shareURL = encodeURI(shareURL);

            var shareWindow = window.open('https://twitter.com/share?url=' + shareURL, 'Share', 'width=500,height=312');
                shareWindow.focus();
        };


        /**
         * Start Facebook share
         */
        $scope.sendFacebookShare = function() {

            /**
             * Get Logindata
             */
            FB.login(function(loginData) {

                if(loginData.authResponse) {
                    // Send FB ID to API
                    $CB_API({
                        url: '/share_images/' + $scope.activeShareImage().id + '/shares',
                        method: 'POST',
                        data: {
                            person: {
                                social_connectors: [{
                                    name: 'facebook',
                                    foreing_id: loginData.authResponse.userID
                                }]
                            }
                        }
                    });

                    // Create API Post
                    var APIPost = {
                        url: $scope.activeShareImage().image_url,
                        message: $scope.facebookShareBody + "\n" + $scope.activeShareImage().facebook_post_link
                    };

                    // Send FB Post
                    FB.api('/me/photos', 'POST', APIPost, function(APIResponse) {
                        if(APIResponse && !APIResponse.error) {
                            // Wallpost success!
                            $scope.showFacebookShare = false;
                            $scope.facebookShareBody = '';
                            $scope.$apply();
                        } else {
                            // Wallpost error fallback
                            FB.ui({
                                method: 'feed',
                                picture: $scope.activeShareImage().image_url,
                                link: $scope.activeShareImage().facebook_post_link,
                                caption: $scope.activeShareImage().title,
                                description: $scope.activeShareImage().description,
                                message: $scope.facebookShareBody
                            });
                        }
                    });
                }

            }, {scope:'email,publish_actions'});
        };

        // Load available images
        $CB_API({
            'url': '/share_images'
        }).done(function(data, status) {
            $scope.shareImages = data.share_images;

            angular.forEach($scope.shareImages, function(shareImage) { setTimeout(function() {
                new Image().src = shareImage.image_url;
            }, 10); });

            $scope.$apply();
        });

    }]);

}());
