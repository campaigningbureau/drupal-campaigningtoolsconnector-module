<div data-ctc-widget="ctc.shareimages" id="ctc--shareimages">
    <div data-ng-controller="shareimages">

        <div class="ctc--shareimages--images">
            <img data-ng-src="{{ activeShareImage().image_url }}">
        </div>

        <div class="ctc--shareimages--controls">
            <div class="pull-left">
                <button class="ctc--shareimages--controls control-select-prev btn btn-primary" data-ng-click="prevShareImage();"><i class="fa fa-fw fa-chevron-left"></i></button>
                <button class="ctc--shareimages--controls control-select-next btn btn-primary" data-ng-click="nextShareImage();"><i class="fa fa-fw fa-chevron-right"></i></button>
            </div>
            <div class="pull-right">
                <button class="ctc--shareimages--controls control-select-facebook btn btn-facebook" data-ng-click="showFacebookShare=true;"><i class="fa fa-fw fa-facebook"></i></button>
                <button class="ctc--shareimages--controls control-select-twitter btn btn-twitter" data-ng-click="sendTwitterShare()"><i class="fa fa-fw fa-twitter"></i></button>
            </div>
        </div>

        <div class="ctc--shareimages--facebook-text" data-ng-show="showFacebookShare">
            <div class="form-group">
                <label class="control-label">Was möchtest Du sagen:</label>
                <textarea class="form-control" data-ng-model="facebookShareBody"></textarea>
            </div>
            <div class="pull-left">
                <button class="ctc-shareimages--controls control-select-back btn btn-primary" data-ng-click="facebookShareBody='';showFacebookShare=false;"><i class="fa fa-fw fa-chevron-left"></i></button>
            </div>
            <div class="pull-right">
                <button class="ctc-shareimages--controls control-select-back btn btn-secondary" data-ng-click="sendFacebookShare()"><i class="fa fa-fw fa-check"></i></button>
            </div>
        </div>

    </div>
</div>
