<?php

/**
 * Get a widget
 *
 * @param string $widget_identifier
 * @param string $language
 * @param bool $use_cache
 * @return string
 */
function ctc_get_widget($widget_identifier, $language = '', $use_cache = true) {
    $widget = NULL;

    if(variable_get('ctc_settings_development_mode', false)) {
        drupal_set_message('Widget cache is turned off', 'warning', false);
        $use_cache = false;
    }

    if(!isset($widget)) {
        $widget = _ctc_get_widget_from_cache(__FUNCTION__ . ':' . $widget_identifier . ':' . $language, $widget_identifier, $language, $use_cache);
    }

    return $widget;
}

/**
 * Get a widget from cache
 *
 * @param string $cache_identifier
 * @param string $widget_identifier
 * @param string $language
 * @param bool $use_cache
 * @return string
 */
function _ctc_get_widget_from_cache($cache_identifier, $widget_identifier, $language = '', $use_cache = true) {
    if(($cache = cache_get($cache_identifier)) && $use_cache) {
        return $cache->data;
    }

    $widget = _ctc_get_widget_from_api($widget_identifier, $language);

    if($use_cache) cache_set($cache_identifier, $widget);

    return $widget;
}

/**
 * @param string $widget_identifier
 * @param string $language
 * @return string
 */
function _ctc_get_widget_from_api($widget_identifier, $language = '') {
    $settings = ctc_get_settings();

    $host = $settings['HOST'];
    $cid = $settings['CID'];

    $url = $host . '/' . $cid . '/widgets/' . $widget_identifier;

    if(!empty($language)) {
        $url .= '?lang=' . $language;
    }

    $response = drupal_http_request($url);

    if($response->code != '200') {
        watchdog('CTC', 'API returned code @code with data @data', array('@code' => $response->code, '@data' => $response->data), WATCHDOG_ERROR, $url);
        return drupal_not_found();
    }

    return $response->data;
}
