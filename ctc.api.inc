<?php

/**
 * insert API Script - but only once
 */
function ctc_add_js_api($print = false) {
    global $ctc_api_in_use;

    if(!$ctc_api_in_use) {
        $ctc_api_in_use = true;

        $settings = ctc_get_settings();
        $apiJSON = array(
            'HOST' => $settings['HOST'],
            'CUSTOMER_IDENTIFIER' => $settings['CID'],
        );

        if(!$print) {
            drupal_add_js($settings['HOST'] . '/' . $settings['CID'] . '/latest/api.js', array(
                'type' => 'external',
                'scope' => 'header',
                'group' => JS_LIBRARY,
                'weight' => 1
            ));

            drupal_add_js('(function() { $CB_API(' . json_encode($apiJSON) . ',true); }());', array(
                'type' => 'inline',
                'scope' => 'footer',
            ));
        } else if($print) {
            print '<script src="' . $settings['HOST'] . '/' . $settings['CID'] . '/latest/api.js"></script>';
            print '<script>(function() { $CB_API(' . json_encode($apiJSON) . ',true); }());</script>';
        }

    }
}

/**
 * Insert resources script
 *
 * it automatically loads the api if not already loaded
 */
function ctc_add_resources($print = false) {
    global $ctc_resources_in_use;

    if(!$ctc_resources_in_use) {
        $ctc_resources_in_use = true;

        $settings = ctc_get_settings();

        $fb_script = file_get_contents(drupal_realpath('module://ctc/fb_template.js'));
        $fb_script = str_replace('{your-app-id}', variable_get('ctc_settings_facebook_app_id'), $fb_script);

        ctc_add_js_api($print);

        if(!$print) {
            drupal_add_js($settings['HOST'] . '/' . $settings['CID'] . '/resources.js', array(
                'type' => 'external',
                'scope' => 'header',
                'group' => JS_LIBRARY,
                'weight' => 0
            ));
            drupal_add_js($settings['HOST'] . '/' . $settings['CID'] . '/resources.tmpl.js', array(
                    'type' => 'external',
                    'scope' => 'footer',
                    'group' => JS_LIBRARY,
                    'weight' => 10
                ));
            drupal_add_css($settings['HOST'] . '/' . $settings['CID'] . '/resources.css', array('type' => 'external'));

            drupal_add_js('(function() { angular.module(\'ct.constant\').run([\'CTConfig\', function(CTConfig) { angular.extend(CTConfig, ' . json_encode(ctc_get_module_identifier()) . ') }]); }());', array(
                'type' => 'inline',
                'scope' => 'footer',
            ));
            drupal_add_js($fb_script, array(
                'type' => 'inline',
                'scope' => 'footer',
            ));
        } else if($print) {
            print '<script src="' . $settings['HOST'] . '/' . $settings['CID'] . '/resources.js"></script>';
            print '<script src="' . $settings['HOST'] . '/' . $settings['CID'] . '/resources.tmpl.js"></script>';
            print '<script>(function() { angular.module(\'ct.constant\').run([\'CTConfig\', function(CTConfig) { angular.extend(CTConfig, ' . json_encode(ctc_get_module_identifier()) . ') }]); }());</script>';
            print '<link rel="stylesheet" href="' . $settings['HOST'] . '/' . $settings['CID'] . '/resources.css" media="all">';
            print '<script>' . $fb_script . '</script>';
        }
    }
}

function ctc_add_widget_bootstrap() {
    global $ctc_widget_bootstrap_in_use;

    if(!$ctc_widget_bootstrap_in_use) {
        $ctc_widget_bootstrap_in_use = true;

//        drupal_add_js(drupal_get_path('module', 'ctc') . '/scripts/ctc-bootstrap-widgets.js', array('scope' => 'footer', 'weight' => 9999));
    }
}
